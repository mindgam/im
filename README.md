# Astra Linux Mattermost deploy

Project for deploy Astra Linux Chat

This project uses information that must be stored in encrypted form (certificates and private key). To use the playbook, you must enter a password for decryption. Password store in wiki-page: [Ansible-ssl-Certs](https://wiki.astralinux.ru/x/1YThBQ)

## Requirements

- Astra Linux CE "Orel" 2.12.29+
- Ansible 2.9+
- Terraform 1.0.0+
- Server with configured network interface and installed ssh keys

## Usage

1. Clone repo and cd into it.

2. If you need to create or modify infrastructure for deployment in Yandex Cloud (YC) then cd into ./terraform-yc inside repo.

   2.1. Edit terraform.tfvars file if you need to modify global cloud parameters.

   2.2. Edit backend.tf file if you need to modify S3 bucket parameters. Don't forget to point "access_key" and "secret_key".

   2.3. You need to create proper "infra-service-im-terraform-bot.json" and "infra-admin.pub" files in ./terraform-yc folder to be able to connect to YC.

   2.4. According to environment you planning to deploy (dev, stage, prod) edit state.name and terraform.tfvars in ./environments/[environment] folder. At state.name file write down name for .tfstate file. At terraform.tfvars fill in infrustructure resources parameters including vm's internal and load balancer external IP addresses.
   Note: since YC doesn't have HTTP target group healthcheck for 443 port yet in "healthcheck_type" and "healthcheck_port" parameters use "tcp" and "443" or "http" and "80" in case of HTTPS and HTTP respectively. You may also use multiple listeners if you need.
   ```YML
   listeners = {
      https = {
         name = "https"
         port = "443"
      }
      http = {
         name = "http"
         port = "80"
      }
   }
   ```

   2.5. Staying in terraform-yc folder run terraform plan initiation.

      `terraform init -backend-config=environments/stage/state.name -reconfigure`

   2.6. If your infrastructure already exists then firstly destroy it.

      `terraform destroy -auto-approve -var-file=./environments/stage/terraform.tfvars`

   2.7. Apply terraform plan.

      `terraform apply -auto-approve -var-file=./environments/stage/terraform.tfvars`

3. Cd into ./ansible inside repo and download dependencies, run

   `ansible-galaxy install -r requirements.yml`

4. Edit inventory path in ansible.cfg to

   `inventory          = ./environments/dev/inventory` - for install on dev environment;

   `inventory          = ./environments/stage/inventory` - for install on stage environment;

   `inventory          = ./environments/prod/inventory` - for deploy Astra Linux chat on production server;

5. For stage, dev and prod install set in environments/[env_name]/inventory ip address and ssh user of a remote machine. Don't change hostname!

5. Edit vars/main.yml - set mattermost version, a strong db pass.

6. If you want restore a backup, set `mm_restore_backup` to true and enter the path to the archive with backup data into the variable `mm_backup_file`. If you want restore a backup with user data, set `mm_download_data_files` to true. The files will start downloading after the playbook is executed in the background. Don't restore backup on working server, backup can be restored on clean install.

7. If you need to add some users for get access via ssh, just set mm_add_ssh_users to True and set a name and public_key of users:
```YML
mm_add_ssh_users: true
mm_ssh_users:
  - name: username
    public_key: "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3oSeK32hKJy6h/5Wb69 username"
```

7. This playbook can update the current version, just specify the update version in the variable `mm_version`. Downgrade not allowed.

8. Run playbook

`ansible-playbook playbooks/site.yml --ask-vault-pass`

Example vars/main.yml:

```YML
# Mattermost version (https://mattermost.com/download/)
# Example:
# mm_version: 5.23.1
mm_version: 5.36.0

# Restore backup
# Example:
# mm_restore_backup: true
mm_restore_backup: false

# Restore backup data files
# Example:
# mm_download_data_files: true
mm_download_data_files: false

# Path to backup file
# Example:
# mm_backup_file: ../backup.tar
mm_backup_file:
```

Example vars for astrabot:

```YML
# Resource address. By default
# artifactory.astralinux.ru
registry:

# Repository name. Default: gca-cr
repository:

# Image title. Default: astrabot
app:

# Image version. Default 1.0
tag:
```

## Bugs

You tell me
