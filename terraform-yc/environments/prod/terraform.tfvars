instance_name             = "prod-im"
zone                      = "ru-central1-a"
cores                     = "4"
platform_id               = "standard-v2"
core_fraction             = "100"
memory                    = "8"
preemptible               = false
first_disk_size           = "200"
auto_delete               = true
im_internal_ip            = "10.177.180.240"
target_group_name         = "prod-im-tg"
load_balancer_name        = "prod-im-lb"
load_balancer_external_ip = "62.84.114.50"
allow_stopping_for_update = false
healthcheck_type          = "tcp"
healthcheck_port          = "443"
listeners = {
  https = {
    name = "https"
    port = "443"
  }
  http = {
    name = "http"
    port = "80"
  }
}
bucket_name = "prod-im-data-storage"
