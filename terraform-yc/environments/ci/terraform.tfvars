instance_name             = "ci-im"
zone                      = "ru-central1-a"
cores                     = "2"
platform_id               = "standard-v2"
core_fraction             = "100"
memory                    = "4"
preemptible               = true
first_disk_size           = "30"
auto_delete               = true
im_internal_ip            = "10.177.180.242"
target_group_name         = "ci-im-tg"
load_balancer_name        = "ci-im-lb"
load_balancer_external_ip = ""
allow_stopping_for_update = true
healthcheck_type          = "tcp"
healthcheck_port          = "443"
listeners = {
  https = {
    name = "https"
    port = "443"
  }
  http = {
    name = "http"
    port = "80"
  }
}
bucket_name = "ci-im-data-storage"
