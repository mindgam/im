instance_name             = "stage-im"
zone                      = "ru-central1-a"
cores                     = "2"
platform_id               = "standard-v2"
core_fraction             = "100"
memory                    = "4"
preemptible               = true
first_disk_size           = "30"
auto_delete               = true
im_internal_ip            = "10.177.180.241"
target_group_name         = "stage-im-tg"
load_balancer_name        = "stage-im-lb"
load_balancer_external_ip = "84.252.131.34"
allow_stopping_for_update = true
healthcheck_type          = "tcp"
healthcheck_port          = "443"
listeners = {
  https = {
    name = "https"
    port = "443"
  }
  http = {
    name = "http"
    port = "80"
  }
}
bucket_name = "stage-im-data-storage"
