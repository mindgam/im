instance_name             = "dev-im"
zone                      = "ru-central1-a"
cores                     = "2"
platform_id               = "standard-v2"
core_fraction             = "5"
memory                    = "4"
preemptible               = true
first_disk_size           = "30"
auto_delete               = true
im_internal_ip            = "10.177.180.243"
target_group_name         = "dev-im-tg"
load_balancer_name        = "dev-im-lb"
load_balancer_external_ip = "51.250.12.66"
allow_stopping_for_update = true
healthcheck_type          = "http"
healthcheck_port          = "80"
listeners = {
  http = {
    name = "http"
    port = "80"
  }
}
bucket_name = "dev-im-data-storage"
