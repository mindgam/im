variable "cloud_id" {
  description = "Cloud ID"
}
variable "folder_id" {
  description = "Folder ID"
}
variable "service_account_key_file" {
  description = "Service account auth key file"
}
variable "image_id" {
  description = "VM image ID"
}
variable "instance_name" {
  description = "Instance name"
}
variable "public_key_path" {
  description = "Public key path for ssh access"
}
variable "preemptible" {
  description = "Preemptible status"
}
variable "first_disk_size" {
  description = "Size for primary disk"
}
variable "auto_delete" {
  description = "Auto delete secondary disk when VM is destroy"
}
variable "subnet_name" {
  description = "Subnet name"
}
variable "im_internal_ip" {
  description = "Internal IP"
}
variable "zone" {
  description = "Zone for infrastucture"
}
variable "target_group_name" {
  description = "Name of target group"
}
variable "load_balancer_name" {
  description = "Name of load balancer"
}
variable "load_balancer_external_ip" {
  description = "EXternal IP"
}
variable "cores" {
  description = "Number of CPU cores"
}
variable "memory" {
  description = "Memory"
}
variable "allow_stopping_for_update" {
  description = "Allow stop instance for update"
}
variable "core_fraction" {
  description = "Core fraction"
}
variable "platform_id" {
  description = "Platform ID"
}
variable "folder_id_interconnect" {
  description = "Foleder ID interconnect"
}
variable "healthcheck_type" {
  description = "Health check type: tcp or http"
}
variable "healthcheck_port" {
  description = "Health check port"
}
variable "listeners" {
  type = map(object({
    name = string
    port = string
  }))
}
variable "bucket_name" {
  description = "Bucket name"
}
variable "bucket_transition_days" {
  description = "Days to transition to cold storage"
  default     = 14
}
variable "access_key" {
  description = "Access key"
}
variable "secret_key" {
  description = "Secret key"
}
