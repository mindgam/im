variable "address" {
  description = "Internal IP for Computer Instance"
}
variable "subnet_id" {
  description = "Subnet ID"
}
variable "target_group_name" {
  description = "Name of target group"
}
