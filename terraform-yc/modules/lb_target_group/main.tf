resource "yandex_lb_target_group" "default" {
  name      = var.target_group_name
  region_id = "ru-central1"
  target {
    subnet_id = var.subnet_id
    address   = var.address
  }
}
