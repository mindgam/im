resource "yandex_storage_bucket" "default" {
  bucket     = var.bucket_name
  access_key = var.access_key
  secret_key = var.secret_key
}
