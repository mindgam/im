variable "bucket_name" {
  description = "Bucket name"
}
variable "bucket_transition_days" {
  description = "Days to transition to cold storage"
  default     = 14
}
variable "access_key" {
  description = "Access key"
}
variable "secret_key" {
  description = "Secret key"
}
