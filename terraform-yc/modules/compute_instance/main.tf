resource "yandex_compute_instance" "default" {
  name                      = var.instance_name
  zone                      = var.zone
  allow_stopping_for_update = var.allow_stopping_for_update
  platform_id               = var.platform_id
  resources {
    cores         = var.cores
    core_fraction = var.core_fraction
    memory        = var.memory
  }
  scheduling_policy {
    preemptible = var.preemptible
  }
  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.first_disk_size
    }
  }
  dynamic "network_interface" {
    for_each = var.network
    content {
      subnet_id  = network_interface.value.subnet_id
      ipv4       = network_interface.value.ipv4
      ip_address = network_interface.value.ip_address
    }
  }
  metadata = {
    user-data = "#cloud-config\nusers:\n  - name: infra-admin\n    groups: astra-admin\n    shell: /bin/bash\n    sudo: ['ALL=(ALL) NOPASSWD:ALL']\n    ssh-authorized-keys:\n      - ${file(var.public_key_path)}"
  }
}
