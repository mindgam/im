variable "network" {
  type = map(object({
    subnet_id  = string
    ipv4       = bool
    ip_address = string
  }))
}
variable "cores" {
  description = "Number of CPU cores for Computer instance"
}
variable "memory" {
  description = "Memory for Computer instance"
}
variable "image_id" {
  description = "VM image ID"
}
variable "instance_name" {
  description = "Instance name"
}
variable "zone" {
  description = "zone"
}
variable "public_key_path" {
  description = "Public key path for ssh access"
}
variable "preemptible" {
  description = "Preemptible property"
}
variable "first_disk_size" {
  description = "Size for primary disk"
}
variable "allow_stopping_for_update" {
  description = "Allow stop instance for update"
}
variable "core_fraction" {
  description = "Core fraction for Computer instance"
}
variable "platform_id" {
  description = "Platform ID for Computer instance"
}
