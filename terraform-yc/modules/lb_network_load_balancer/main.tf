resource "yandex_lb_network_load_balancer" "tcp" {
  count = var.healthcheck_type == "tcp" ? 1 : 0
  name  = var.load_balancer_name
  dynamic "listener" {
    for_each = var.listeners
    content {
      name = listener.value.name
      port = listener.value.port
      external_address_spec {
        ip_version = "ipv4"
        address    = var.load_balancer_external_ip
      }
    }
  }
  attached_target_group {
    target_group_id = var.target_group_id
    healthcheck {
      name = var.healthcheck_type
      tcp_options {
        port = var.healthcheck_port
      }
    }
  }
}

resource "yandex_lb_network_load_balancer" "http" {
  count = var.healthcheck_type == "http" ? 1 : 0
  name  = var.load_balancer_name
  dynamic "listener" {
    for_each = var.listeners
    content {
      name = listener.value.name
      port = listener.value.port
      external_address_spec {
        ip_version = "ipv4"
        address    = var.load_balancer_external_ip
      }
    }
  }
  attached_target_group {
    target_group_id = var.target_group_id
    healthcheck {
      name = var.healthcheck_type
      http_options {
        port = var.healthcheck_port
      }
    }
  }
}
