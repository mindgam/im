variable "target_group_id" {
  description = "ID of target group"
}
variable "load_balancer_name" {
  description = "Name of load balancer"
}
variable "load_balancer_external_ip" {
  description = "External IP for load balancer"
}
variable "healthcheck_type" {
  description = "Health check type: tcp or http"
}
variable "healthcheck_port" {
  description = "Health check port"
}
variable "listeners" {
  type = map(object({
    name = string
    port = string
  }))
}
