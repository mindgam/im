provider "yandex" {
  service_account_key_file = pathexpand(var.service_account_key_file)
  cloud_id                 = var.cloud_id
  folder_id                = var.folder_id
}
data "yandex_vpc_subnet" "default" {
  name      = var.subnet_name
  folder_id = var.folder_id_interconnect
}
data "yandex_compute_instance" "default" {
  name       = var.instance_name
  depends_on = [module.compute_instance]
}
data "yandex_lb_target_group" "default" {
  name       = var.target_group_name
  depends_on = [module.compute_instance, module.lb_target_group]
}
module "compute_instance" {
  source                    = "./modules/compute_instance"
  platform_id               = var.platform_id
  allow_stopping_for_update = var.allow_stopping_for_update
  cores                     = var.cores
  core_fraction             = var.core_fraction
  memory                    = var.memory
  image_id                  = var.image_id
  first_disk_size           = var.first_disk_size
  preemptible               = var.preemptible
  instance_name             = var.instance_name
  public_key_path           = var.public_key_path
  zone                      = var.zone
  network = {
    int_network = {
      subnet_id  = data.yandex_vpc_subnet.default.id
      ipv4       = true
      ip_address = var.im_internal_ip
    }
  }
}
module "lb_target_group" {
  source            = "./modules/lb_target_group"
  target_group_name = var.target_group_name
  subnet_id         = data.yandex_vpc_subnet.default.id
  address           = data.yandex_compute_instance.default.network_interface.0.ip_address
  depends_on        = [module.compute_instance]
}
module "lb_network_load_balancer" {
  source                    = "./modules/lb_network_load_balancer"
  load_balancer_name        = var.load_balancer_name
  target_group_id           = data.yandex_lb_target_group.default.id
  load_balancer_external_ip = var.load_balancer_external_ip
  healthcheck_type          = var.healthcheck_type
  healthcheck_port          = var.healthcheck_port
  listeners                 = var.listeners
  depends_on                = [module.lb_target_group]
}

module "storage_bucket" {
  source                 = "./modules/storage_bucket"
  bucket_name            = var.bucket_name
  bucket_transition_days = var.bucket_transition_days
  access_key             = var.access_key
  secret_key             = var.secret_key
}
